function detectOS() {
    const platform = navigator.platform.toLowerCase();

    if (platform.includes('mac')) return 'MacOS';
    if (platform.includes('win')) return 'Windows';
    if (/linux/.test(platform)) return 'Linux';

    return 'unknown';
}

function change(software){
    document.getElementById('status').innerHTML = 'Install for ' + software;
}


var ops = detectOS()
change(ops);