const { app, BrowserWindow, Menu } = require('electron');

const os = require('os-utils');
const url =  require('url');
const path = require('path');

if (process.env.NODE_ENV != 'production'){
    require('electron-reload')(__dirname, {
        electron: path.join(__dirname, '../node_modules', '.bin', 'electron')
    });
}

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        width: 860,
        height: 690,
        icon: __dirname + '/views/icon/macarena.ico',
        webPreferences: {
            nodeIntegration: true
        }
    });

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views/index.html'),
        protocol: 'file',
        slashes: 'true',
    }));

    os.cpuUsage(function(){
        software = operativeSystem()
        mainWindow.webContents.send('os',software);
        console.log('WORKING...')
    });

    function operativeSystem() {
        if (process.platform == 'win32') return 'Windows'
        if (process.platform == 'linux') return 'Linux'
        if (process.platform == 'darwin') return 'MacOS'
        
        return 'Unknown'
    }

    Menu.setApplicationMenu(null);

});