chcp utf-8
@echo off
cls
echo MACARENA LIBRARY WINDOWS INSTALLER.
echo.
set /p option="Desea instalar esta libreria? (Si / No): "
echo.
if %option%==si (
    goto install
) else (
    if %option%==Si (
        goto install
    ) else (
        if %option%==s (
            goto install
        ) else (
            if %option%==S (
                goto install
            ) else (
                if %option==SI (
                    goto install
                ) else (
                    set result=cancelada.
                    goto end
                )
            )        
        )    
    )
)

:install
xcopy ..\src\macarena.py C:\python38\lib /s
echo.
pause
set result=exitosa.

:end
echo.
echo Instalacion %result%